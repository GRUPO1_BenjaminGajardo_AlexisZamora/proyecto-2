#!usr/bin/python
from RPi_GPIO_i2c_LCD import lcd    #lib para pantalla LCD
import serial                       #lib para comunicacion serial con arduino
import Adafruit_DHT                 #lib para obtener mediciones DHT11
import time                         #lib para esperar un tiempo y obtener la hora local
import psycopg2                     #lib para ingresar datos a postgresql
import threading                    #lib para ejecutar distintos metodos simultaneamente
import matplotlib.pyplot as plt     #lib para graficar
import socket                       #lib para creacion de sockets

#LCD
i2c_address = 0x27
lcdDisplay = lcd.HD44780(i2c_address)
#DHT11
sensorHT = Adafruit_DHT.DHT11
pinHT = 4
#socket
FORMAT = 'utf-8'
HOST = '192.168.1.158'
PORT = 8080

lcdDisplay.backlight("on")
print('inicializando...')


class Ardu: #Declaración de clase Ardu


    def __init__(self, port, bauds): #Método Constructor: establece el puerto y la tasa de baudios
        self.port = port      #Argumento Self (uno mismo) para el puerto
        self.bauds = bauds    #Argumento Self (uno mismo) para los baudios
        self.humedad, self.temperatura = Adafruit_DHT.read(sensorHT, pinHT)
        self.polution = None
        self.gas = None

    def getData(self): #Metodo getData (recibe la información de arduino y la muestra por pantalla)

        #objeto serial (de biblioteca serial) se le pasa el objeto Arduino con sus atributos port y bauds
        ser = serial.Serial(Arduino.port, Arduino.bauds)
        ser.flushInput() #descarta lo contenido en el buffer serial
        palabra = '' #se declara la variable

        while True: #repetir en bucle
            #codigo a ejecutar, pero podrian presentarse errores
            try:

                Arduino.humedad, Arduino.temperatura = Adafruit_DHT.read_retry(sensorHT, pinHT)

                #Lectura de datos que provienen del arduino, retorna un byte a la vez
                readByte = ser.read()
                #se decodifica por utf-8, convierte bytes de arduino a cadena de texto (string)
                line = readByte.decode('utf-8')
                #completa la cadena de texto "palabra" con los valores de line
                

                #Logica de obtencion de datos serial, solo se ingresara a este bloque de codigo si el string palabra termina con "<"
                if line == "<":
                    Arduino.polution = palabra
                    palabra=''
                    line = ''
                if line == ">":
                    Arduino.gas = palabra
                    palabra=''
                    line = ''
                    print("["+threading.current_thread().getName()+"]" ,"Datos:\nTemperatura: "+str(Arduino.temperatura)+" °C\nHumedad: "+str(Arduino.humedad)+ "%\nPolucion: "+str(Arduino.polution)+"pcs/0.01cf\nConcentracion CO2: "+str(Arduino.gas)+" ppm")

                palabra += line

                lcdDisplay.set("Temp: "+str(Arduino.temperatura)+" °C", 1)      #Mostrar mediciones en pantalla LCD
                lcdDisplay.set("Humedad: "+str(Arduino.humedad)+"%", 2)

            except KeyboardInterrupt: #el programa se detiene con CTRL+C (interrupcion teclado)
                print("["+threading.current_thread().getName()+"]" ,'saliendo...')
                lcdDisplay.backlight("off")
                break

class postgres: #Declaración de clase postgres

    def __init__(self, host, database, user, password): #Método Constructor: establece los parametros a utilizar

        self.host = host            #Argumento Self (uno mismo) para la direccion del host del servidor SQL
        self.database = database    #Argumento Self (uno mismo) para el nombre de la base de datos SQL
        self.user = user            #Argumento Self (uno mismo) para el usuario que se conecta a la base de datos
        self.password = password    #Argumento Self (uno mismo) para la clave asociada al usuario SQL

    def connect(self):
        conn = None
        time.sleep(60)              #Espera a que se realice la conexion y recepcion de los datos por parte de Arduino

        try:
            print("["+threading.current_thread().getName()+"]" ,"Empezando la conexion con la base de datos")
            conn = psycopg2.connect(host = database.host, database = database.database, user = database.user, password = database.password)
            cur = conn.cursor()
            print("["+threading.current_thread().getName()+"]" ,"Conexion con la base de datos establecida correctamente.")

            while True:
                # Obtencion de mediciones del objeto Arduino
                data = 'temperatura= '+str(Arduino.temperatura)+', Humedad= '+str(Arduino.humedad)+', polucion= '+str(Arduino.polution)+', CO2 = '+str(Arduino.gas)
                # Guardar en DB
                if Arduino.temperatura is not None and Arduino.gas is not None:
                    consulta = "INSERT INTO mediciones (arduino ,temperatura, humedad, polucion, CO2) VALUES (1 ,'"+str(Arduino.temperatura)+"', '"+str(Arduino.humedad)+"', '"+str(Arduino.polution)+"', '"+str(Arduino.gas)+"')"
                    cur.execute(consulta)       #ejecuta la consulta
                    conn.commit()               #hace valida la consulta
                time.sleep(5)

        # Errores que se pueden presentar, los cuales son informados
        except (Exception, psycopg2.DatabaseError) as error:
            print("["+threading.current_thread().getName()+"]" ,error)
        finally:
            if conn is not None:
                conn.close()
                print("["+threading.current_thread().getName()+"]" ,'La conexión con la base de datos ha sido terminada')

class view():
    string_temperatura = []     # inicializacion de los arreglos que se utilizaran para realizar el ploteo de datos
    string_humedad = []
    string_polution = []
    string_co2 = []
    string_tiempo = []

    zonahoraria = time.localtime()                   # Obtencion de la hora local del equipo
    dia = time.strftime("%b %d %Y", zonahoraria)     # Formato de hora (Mes, dia, año)

    def string_datos():
        i = 0
        time.sleep(180)
        while True:
            zonahoraria = time.localtime()                     
            hora = time.strftime("%H:%M", zonahoraria)     # Formato de hora para los graficos (hora, minutos)

            view.string_tiempo.append(hora)                         # Ingreso de las mediciones en el objeto Arduino en los arreglos antes creados
            view.string_temperatura.append(Arduino.temperatura)     # append para que se ingrese en el ultimo puesto libre del arreglo
            view.string_humedad.append(Arduino.humedad)
            view.string_polution.append(Arduino.polution)
            view.string_co2.append(Arduino.gas)

            time.sleep(300)  #muestras cada 5 min

    def plot1():    #Graficos correspondientes a temperatura y humedad
      
        # Creacion de 4 sub-graficos en un solo grafico principal (2x1)
        figura1, axis1 = plt.subplots(2, 1)
        
        # plot temperatura
        axis1[0].plot(view.string_tiempo, view.string_temperatura)
        axis1[0].set_title("Temperatura [°C]")
        
        # plot humedad
        axis1[1].plot(view.string_tiempo, view.string_humedad)
        axis1[1].set_title("Humedad [%]")

        figura1.suptitle('Mediciones durante: '+view.dia, fontsize=15)
        plt.tight_layout()  # previene overlaps de titulos
       
        # muestra y guarda los subplots
        plt.savefig("/plots/plots1.png")
        plt.show()
        print("["+threading.current_thread().getName()+"]" ,"Se generó y guardó el grafico correspondiente a temperatura y humedad")


    def plot2():    # Graficos correspondientes a polucion y concentracion de CO2

        # Creacion de 4 sub-graficos en un solo grafico principal (2x1)
        figura2, axis2 = plt.subplots(2, 1)

        # plot polucion
        axis2[0].plot(view.string_tiempo, view.string_polution)
        axis2[0].set_title("Polucion [pcs/0.01cf]")
        
        # plot CO2
        axis2[1].plot(view.string_tiempo, view.string_co2)
        axis2[1].set_title("Concentracion de CO2 [ppm]")

        figura2.suptitle('Mediciones durante: '+view.dia, fontsize=15)
        plt.tight_layout()  # previene overlaps de titulos

        # muestra y guarda los subplots
        plt.savefig("/plots/plots2.png")
        plt.show()
        print("["+threading.current_thread().getName()+"]" ,"Se generó y guardó el grafico correspondiente a polución y concentración de CO2")

def sender(s):  # metodo para enviar informacion a traves del socket cliente hacia el socket server

    try:
        print("["+threading.current_thread().getName()+"]" ,"enviando datos")
        while True:
                data ="Temperatura: "+str(Arduino.temperatura)+" °C\nHumedad: "+str(Arduino.humedad)+ "%\nPolucion: "+str(Arduino.polution)+"pcs/0.01cf\n"
                s.send(data.encode(FORMAT))
                data = "Concentracion CO2: "+str(Arduino.gas)+" ppm"
                s.send(data.encode(FORMAT))
                time.sleep(10)

    finally:
        s.close()

# Socket del cliente
def socket_cliente():
    while True:
        try:
            s = socket.socket()
            s.connect((HOST, PORT))
            print("["+threading.current_thread().getName()+"]" ,"Conexion con el servidor socket establecida.")
            threading.Thread(name='hilo_envio',target=sender, args=(s,)).start()
            while True:
                data = s.recv(2048).decode(FORMAT)
                print("["+threading.current_thread().getName()+"]" ,data)
                time.sleep(10)

        except:
            print("["+threading.current_thread().getName()+"]" ,"No se encuentra el socket del servidor para enviar los datos.")
            print("["+threading.current_thread().getName()+"]" ,"Reconectando con el servidor en 30 segundos.")
            time.sleep(30)


# Creacion de objetos e hilos
plot = view()
Arduino = Ardu("/dev/ttyACM0", 9600) #Creación del objeto Arduino
database = postgres('192.168.1.158', 'postgres', 'postgres', 'Admin.123') #Creacion del objeto database


t1 = threading.Thread(name='hilo_Arduino', target=Arduino.getData).start()     #Ejecucion de hilos simulaneamente,
t2 = threading.Thread(name='hilo_PostgreSQL', target=database.connect).start() #correspondientes a los metodos de cada clase
t3 = threading.Thread(name='hilo_Graficos', target=view.string_datos).start()  
time.sleep(10)
t4 = threading.Thread(name='hilo_socket', target=socket_cliente).start()