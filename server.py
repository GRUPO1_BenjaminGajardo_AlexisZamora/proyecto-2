import socket
import threading

HEADER = 64
PORT = 8080
SERVER = '192.168.0.158'
ADDR = (SERVER, PORT)
FORMAT = 'utf-8'
DISCONNECT_MESSAGE = "!DISCONNECT"
#####
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #Creacion de un nuevo socket.
server.bind(ADDR)

def handle_client(conn, addr):
    print(f"[Nueva conexión] {addr} conectado.")

    connected = True
    #conexion condicion de entrada
    while connected:
        msg_length = conn.recv(HEADER).decode(FORMAT)#HEDEAR how many bytes we recive
        if msg_length:#if the message has some content
            print(msg_length)
            #conn.send("Msg received".encode(FORMAT))

    conn.close()


def start():
    server.listen()
    print(f"[ESCUCHA] El servidor se encuentra en escucha en la dirección {ADDR}")
    while True:
        conn, addr = server.accept()#espera aca hasta que ocurra una nueva conexión donde guarda el objeto socket y el address el cual nos permite
        #                               comunicaros hace el cliente
        #ahora declaramos un threath para manejar la conección de ese cliente
        thread = threading.Thread(target=handle_client, args=(conn, addr))
        thread.start()
        #imprimir cuantas conexiónes activas tenemos
        print(f"[CONEXIONES ACTIVAS] {threading.active_count() - 1}")


print("[INICIALIZANDO] Servidor iniciando...")
start()
